package jp.co.junit4.sample;

public class FizzBuzz {

	public String response(int i) {
		//3と5で割り切れた場合
		if ((i % 3 == 0) && (i % 5 == 0)) {
			return "FizzBuzz";
		}
		//3で割り切れた場合
		if (i % 3 == 0) {
			return "Fizz";
		}
		//5で割り切れた場合
		if (i % 5 == 0) {
			return "Buzz";
		}

		//3と5の倍数以外の場合
		return String.valueOf(i);
	}

}
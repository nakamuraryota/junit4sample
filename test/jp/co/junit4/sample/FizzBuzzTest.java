package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FizzBuzzTest {
	FizzBuzz fizzbuzz = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		fizzbuzz = new FizzBuzz();
		System.out.println("setUp");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	@Test
	public void 引数3の倍数を与えたらFizz() {
		assertEquals("Fizz",fizzbuzz.response(3));
		assertEquals("Fizz",fizzbuzz.response(12));
		assertEquals("Fizz",fizzbuzz.response(-12));
	}


	@Test
	public void 引数5の倍数を与えたらBuzz() {
		assertEquals("Buzz",fizzbuzz.response(5));
		assertEquals("Buzz",fizzbuzz.response(20));
		assertEquals("Buzz",fizzbuzz.response(-20));
	}


	@Test
	public void 引数3と5の倍数を与えたらFizzBuzz(){
		assertEquals("FizzBuzz",fizzbuzz.response(15));
		assertEquals("FizzBuzz",fizzbuzz.response(300));
		assertEquals("FizzBuzz",fizzbuzz.response(-300));
	}

	@Test
	public void 引数が3と5の倍数以外の場合そのままの数字を返す(){
		assertEquals("1",fizzbuzz.response(1));
		assertEquals("17",fizzbuzz.response(17));
		assertEquals("-22",fizzbuzz.response(-22));
	}
}